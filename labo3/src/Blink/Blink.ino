#include <PCD8544.h>

int LED_PIN = 13;
int MODO_PIN = 8; 
int CANAL_1_PIN = A0;
int CANAL_3_PIN = A2;
int val = 0;
int counter = 0;
int voltaje_1 = 0;
static PCD8544 lcd;
int modo = 0;
float R1 = 100000;
float R2 = 71428;
float v = 0;
float v2 = 0;
int array_canales[] = {A0, A1, A2, A3};
int canal = 0;
void setup() {                
  lcd.begin();
	pinMode(LED_PIN, OUTPUT);  
  pinMode(MODO_PIN , INPUT); 
  Serial.begin(9600);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print ("4 Channel Voltmeter");
}

uint16_t get_max(int canal) {
  uint16_t max_v = 0;
  for(uint8_t i = 0; i < 100; i++) {
    uint16_t r = analogRead(canal);  // read from analog channel 3 (A3)
    if(max_v < r) max_v = r;
    delayMicroseconds(200);
  }
  return max_v;
}
void loop() {
  modo = digitalRead(MODO_PIN);
  if (modo == 0) {
  //Voltimetro en DC
    for (int i = 0; i <= 3; i++) {
      canal = array_canales[i];
      v = analogRead(canal) * (5 / 1023.0);
      v2 = v / (R2 / (R1 + R2)); 
      lcd.setCursor (0,i+1) ;
      lcd.print ("V") ;
      lcd.print (i) ;
      lcd.print (":  ") ;
      lcd.print (v2) ;
      lcd.print ("V") ;
    }
  } 
  else {
  //Voltimetro en AC
      for (int i = 0; i <= 3; i++) {
        canal = array_canales[i];
        uint32_t v = get_max(canal);
        v = v * (5 / 1023.0);
        v2 = v / (R2 / (R1 + R2)); 
        v2 /= sqrt(2);
        lcd.setCursor (0,i+1) ;
        lcd.print ("V") ;
        lcd.print (i) ;
        lcd.print (":  ") ;
        lcd.print (v2) ;
        lcd.print ("Vrms") ;
      }
  }
  
  if (v2 > 11.90 || v2 < -11.90) {
    digitalWrite(LED_PIN, HIGH);
  }
  else {
    digitalWrite(LED_PIN, LOW);
  }
  delay(200) ;
}
